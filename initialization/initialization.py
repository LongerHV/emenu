import json
from pathlib import Path
from typing import List, Mapping, Union
import requests
import argparse
import urllib.parse


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='emenu data initialization')
    parser.add_argument('--host', type=str, default='localhost', help='emenu server hostname')
    parser.add_argument('--port', type=int, default=8080, help='emenu server port')
    parser.add_argument('--username', type=str, default='admin', help='emenu username')
    parser.add_argument('--password', type=str, default='admin', help='emenu password')
    parser.add_argument(
        '--file', type=Path, default=Path('initialization_data.json'), help='Initialization file'
    )
    return parser.parse_args()


def load_init_data(file_path: Union[str, Path]) -> List[Mapping]:
    with open(file_path, 'r') as f:
        return json.load(f)


def initialize(args: argparse.Namespace, data: List[Mapping]):
    session = requests.Session()
    url = f'http://{args.host}:{args.port}'
    auth_data = urllib.parse.urlencode({'username': args.username, 'password': args.password})
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = session.post(f'{url}/token', data=auth_data, headers=headers)
    response.raise_for_status()
    headers = {'Authorization': f'Bearer {response.json()["access_token"]}'}
    for card in data:
        response = session.post(f'{url}/card', json=card, headers=headers)
        if not response.ok:
            print(response.text)


if __name__ == '__main__':
    args = parse_arguments()
    data = load_init_data(args.file)
    initialize(args, data)
