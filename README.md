# emenu

[![pipeline status](https://gitlab.com/LongerHV/emenu/badges/main/pipeline.svg)](https://gitlab.com/LongerHV/emenu/-/commits/main)
[![coverage report](https://gitlab.com/LongerHV/emenu/badges/main/coverage.svg)](https://gitlab.com/LongerHV/emenu/-/commits/main)

[[_TOC_]]

## Overview

API serving as an online menu card.

### Demonstration instance

There is a demonstration instance deployed in
[Google Cloud](http://emenu.crabdance.com/),
feel free to view it.

### Dependencies

These run dependencies are automatically installed by `pip`.

- [fastapi](https://fastapi.tiangolo.com/) - Web framework
- [uvicorn](http://www.uvicorn.org/) - ASGI server
- [SQLAlchemy](https://www.sqlalchemy.org/) - SQL toolkit
- [pydantic](https://pydantic-docs.helpmanual.io/) - Data validation
- [psycopg2-binary](https://www.psycopg.org/) - PostgreSQL driver
- [argon2-cffi](https://argon2-cffi.readthedocs.io/en/stable/) - Password hashing
- [python-jose](https://python-jose.readthedocs.io/en/latest/) - Sign tokens
- [pbr](https://docs.openstack.org/pbr/latest/) - Manage version number

### Configuration

Some parameters of the program can be configured with environment variables.

Available settings:

#### General

- EMENU_SECRET_KEY - Secret key used for token signing
- EMENU_DEFAULT_USER - Default API user username
- EMENU_DEFAULT_PASS - Default API user password
- EMENU_DEFAULT_MAIL - Default API user email

#### PostgreSQL

- EMENU_PG_ENABLE - Enable PostgreSQL driver
- EMENU_PG_HOST - PostgreSQL hostname
- EMENU_PG_PORT - PostgreSQL port
- EMENU_PG_USER - PostgreSQL username
- EMENU_PG_PASS - PostgreSQL password

#### SMTP

- SMTP_ENABLE - Enable SMTP support
- SMTP_HOST - Address of SMTP server
- SMTP_PORT - SMTP server port
- SMTP_USER - SMTP username
- SMTP_PASS - SMTP user password
- SMTP_SENDER - SMTP sender email address
- MAIL_TIME - Time of daily email updates (iso format)

### Database

There are 3 possible database configurations:

#### SQLite (default)

Use database in a file.

#### SQLite in-memory (tests only)

Use database in RAM. Fast, volatile, easy to restore to initial state.

#### PostgreSQL (external)

Use external database.

## Working with binaries

There are pre-build binaries available at gitlab.
You can use read-only deploy token to access these packages.

### PyPI

Install Python package from gitlab PyPI repository.

```bash
pip install emenu --extra-index-url https://__token__:1q3DiTaf5XxNRoz_bsLY@gitlab.com/api/v4/projects/27774809/packages/pypi/simple
```

With application installed, you can run it locally.

```bash
uvicorn emenu.main:app --reload --port 8080 --host 0.0.0.0
```

### Docker registry

Pull and run docker image from gitlab registry.

```bash
docker login -u gitlab+deploy-token-511119 -p 1q3DiTaf5XxNRoz_bsLY registry.gitlab.com
docker pull registry.gitlab.com/longerhv/emenu:latest
docker run -p 8080:80 registry.gitlab.com/longerhv/emenu:latest
```

## Working with sources

All common commands used for development are wrapped as `make targets`.
Most of them will use automatically created `venv` to install dependencies.

### Clone the repo

```bash
https://gitlab.com/LongerHV/emenu.git
cd emenu
```

### Run tests

`test_all` target will run `flake8`, `mypy` and `pytest` testsuite.

```bash
make test_all
```

### Install

Use `install` target to install the package from source.

```bash
make install
```

### Develop

`develop` target will install the application in **editable** mode.

```bash
make develop
```

### Run locally

`Makefile` contains a `run_local` target, which can be used to run application locally.
This will run the application with an embedded `SQLite` database.

```bash
make run_local
```

### Build Python package

It is possible to build **wheel** and **source** packages with `make build` target.
Freshly created packages will be put inside `dist` directory.

```bash
make build
```

### Run in docker

Use `make docker_run` to build an image and run a dockerized version of the application.

```bash
make docker_run
# Or run in the backgound
make docker_run_daemon
```

### Build docker image

Use `make docker_build` target to build docker image from source.
Resulting image will be tagged with `registry.gitlab.com/longerhv/emenu:<version>`
and `registry.gitlab.com/longerhv/emenu:latest`.

```bash
make docker_build
```

### Run docker-compose

`Docker-compose` allows to run multi-container applications locally.
Predefined configuration uses `PostgreSQL` database running in a separate container.
Additional container with fake SMTP server is also available in this configuration.

```bash
make docker_compose
```

## Pipeline

Gitlab pipelite is used to test, build and deploy a project.

### Validate

These jobs are scheduled on every push.

#### flake8

Check for linting errors.

#### mypy

Check for type errors.

#### pytest

Run tests and generate coverage info.

### Build

These jobs are scheduled on tag push only.

#### gitlab-docker

Build docker image and push to gitlab registry.

#### gitlab-pypi

Build pip package and push to gitlab repository.

### Deploy

Apply a deployment and service configuration
to release a new version in GKE (Google Cloud).

## References

- [Use gitlab registry in GKE](
https://estl.tech/using-images-from-a-private-registry-on-gke-b3bfb2562b16?gi=f219e2eb3b7f)
