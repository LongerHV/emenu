ARG BASE_IMAGE=python:3.9-alpine3.14
FROM $BASE_IMAGE as initial

COPY .git /app/.git

WORKDIR /app

# Install git and checkout repository
RUN apk update
RUN apk add postgresql-dev python3-dev libffi-dev alpine-sdk cargo
RUN git checkout .

# Install emenu and dependencies to /install directory
RUN mkdir /install
RUN python -m pip install --use-feature=in-tree-build --prefix /install --no-cache-dir .

# Base on fresh image
FROM $BASE_IMAGE

# Install PostgreSQL libraries needed by psycopg driver
RUN apk add libpq

# Copy packages installed in intermediate layers
COPY --from=initial /install /usr/local

# Add non-root user
ENV DOCKER_USER=serveruser
ENV HOME_DIR=/localhome

RUN adduser -D -h $HOME_DIR $DOCKER_USER

USER $DOCKER_USER
WORKDIR $HOME_DIR

EXPOSE 8080

CMD ["python", "-m", "uvicorn", "emenu.main:app", "--port", "8080", "--host", "0.0.0.0"]
