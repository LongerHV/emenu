fastapi
pydantic[email]
uvicorn[standard]
SQLAlchemy
argon2-cffi
python-multipart
python-jose[cryptography]
psycopg2-binary
