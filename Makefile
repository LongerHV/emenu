package_name = $(shell python setup.py --name)
package_version = $(shell python setup.py --version)
registry = registry.gitlab.com/longerhv
image_tag = $(registry)/$(package_name):$(package_version)
latest_tag = $(registry)/$(package_name):latest


venv:
	[ -d .venv ] && return 0 || python -m venv .venv && ln -fs .venv/bin/activate activate && . ./activate && python -m pip install -r requirements.txt -r build-requirements.txt -r lint-requirements.txt -r test-requirements.txt

clean:
	rm -rf dist build **/__pycache__ .eggs *.egg-info **/*.pyc .pytest_cache **/.coverage .coverage *.db .mypy_cache

test_all: venv
	. ./activate && flake8 && mypy && bandit -r $(package_name) && pytest

install: venv
	. ./activate && python setup.py install

develop: venv
	. ./activate && python -m pip install pynvim autopep8 flake8 && python setup.py develop

build: venv
	. ./activate && python setup.py sdist bdist_wheel

run_local: venv
	. ./activate && uvicorn emenu.main:app --reload --port 8080 --host 0.0.0.0

docker_build:
	docker build -t $(image_tag) . && docker tag $(image_tag) $(latest_tag)

docker_run: docker_build
	docker run -p 8080:8080 -it $(image_tag)

docker_run_daemon: docker_build
	docker run -p 8080:8080 -d -it $(image_tag)

docker_compose: docker_build
	docker-compose up -d

cloc:
	cloc --quiet --vcs git .

coverage: venv
	. ./activate && python -m coverage run --omit='.venv/*' -m pytest && python -m coverage report -m
