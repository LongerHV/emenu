from datetime import datetime
from typing import Dict, List, Mapping, Optional
import unittest

from emenu import database, models


class TestCommon(unittest.TestCase):
    def setUp(self) -> None:
        """Create all tables in database."""
        models.Base.metadata.create_all(bind=database.engine)

    def tearDown(self) -> None:
        """Dispose connection pool to reset database state."""
        database.engine.dispose()

    @property
    def user(self) -> Dict:
        """Example user to be used in tests."""
        return {
            'username': 'testuser',
            'password': 'Passw0rd1',
            'email': 'test@mail.dom'
        }

    @property
    def dish(self) -> Dict:
        """Example dish to be used in tests."""
        return {
            'name': self.id(),
            'description': self.shortDescription(),
            'price': 30.0,
            'vegetarian': False,
            'preparation_time': 15,
        }

    @property
    def card(self) -> Dict:
        """Example card to be used in tests."""
        return {
            'name': self.id(),
            'description': self.shortDescription(),
        }

    @property
    def time_point(self) -> List[datetime]:
        return [
            datetime(2012, 12, 21),
            datetime(2015, 6, 6),
            datetime(2018, 1, 1)
        ]

    def assertIsSubset(self, superset: Mapping, subset: Mapping, msg: Optional[str] = None):
        """Check if one dictionary is a subset of the other one."""
        msg = msg or f'Dict {str(subset)} is not a subset of {str(superset)}'
        self.assertTrue(all(item in superset.items() for item in subset.items()), msg)
