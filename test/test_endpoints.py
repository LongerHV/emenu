from datetime import datetime
from typing import Dict
import urllib.parse

from fastapi.testclient import TestClient
from freezegun import freeze_time
from requests.models import Response

from emenu import crud, database, schemas
from emenu.main import app
from test.test_common import TestCommon


class EndpointsTestCase(TestCommon):
    client: TestClient

    @classmethod
    def setUpClass(cls):
        """Construct test client."""
        cls.client = TestClient(app)

    def setUp(self) -> None:
        """Create test user in database and get access token."""
        super().setUp()
        username = 'testuser'
        password = 'Passw0rd1'
        email = 'test@mail.dom'

        user = schemas.UserCreate(username=username, password=password, email=email)
        with database.SessionLocal() as db:
            crud.create_user(db, user)

        access_token = self.get_access_token(username, password)
        self.headers = {'Authorization': f'Bearer {access_token}'}

    def get_access_token(self, username: str, password: str) -> str:
        """Login to server to generate access token."""
        data = urllib.parse.urlencode({'username': username, 'password': password})
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        response = self.client.post('/token', data=data, headers=headers)
        self.assertTrue(response.ok, response.text)
        return response.json()['access_token']

    def create_dish(self, dish: Dict) -> Response:
        """Create a single dish in a database."""
        response = self.client.post('/dish', json=dish, headers=self.headers)
        self.assertTrue(response.ok, response.text)
        self.assertIsSubset(response.json(), dish)
        return response

    def test_create_dish(self):
        """Create a dish and verify response."""
        response = self.create_dish(self.dish)
        dish_json = response.json()

        self.assertEqual(dish_json['creation_time'], dish_json['update_time'])

        creation_time = dish_json['creation_time']
        datetime.fromisoformat(creation_time)

        response = self.client.get(f'/dish/{dish_json["id"]}', headers=self.headers)
        self.assertTrue(response.ok, response.text)
        self.assertEqual(dish_json, response.json(), 'Created and gotten dishes are not equal')

    def test_get_dishes(self):
        """Get all dishes."""
        dishes = []
        for _ in range(5):
            dishes.append(self.create_dish(self.dish).json())
        response = self.client.get('/dishes')
        self.assertTrue(response.ok, response.text)
        self.assertEqual(dishes, response.json())

    def test_create_dish_invalid(self):
        """Create dish with invalid `price` and `preparation_time`"""
        dish = self.dish
        dish['price'] = -10
        dish['preparation_time'] = -10
        response = self.client.post('/dish', json=dish)
        self.assertFalse(response.ok, response.text)

    def test_create_dish_incomplete(self):
        """Attempt to create dish with no name parameter."""
        dish = self.dish.copy()
        dish.pop('name')
        response = self.client.post('/dish', json=dish, headers=self.headers)
        self.assertFalse(response.ok, response.text)
        self.assertEqual(
            response.text,
            '{"detail":[{"loc":["body","name"],'
            '"msg":"field required","type":"value_error.missing"}]}'
        )

    def test_update_dish(self):
        """Create dish, modify, verify changes."""
        response = self.create_dish(self.dish)

        original_time = response.json()['update_time']

        update = {
            'price': 24.99,
            'vegetarian': True,
            'preparation_time': 45,
        }
        url = f'/dish/{response.json()["id"]}'
        response = self.client.put(url, json=update, headers=self.headers)
        resp_json = response.json()

        self.assertTrue(response.ok, response.text)
        self.assertIsSubset(resp_json, {**self.dish, **update})
        self.assertNotEqual(original_time, resp_json['update_time'], 'Time not updated')

    def test_update_dish_invalid(self):
        dish = self.dish
        response = self.client.post('/dish', json=dish, headers=self.headers)
        self.assertTrue(response.ok, response.text)
        dish_id = response.json()['id']
        update = {'price': -10, 'preparation_time': -10}
        response = self.client.put(f'/dish/{dish_id}', json=update)
        self.assertFalse(response.ok, response.text)

    def test_delete_dish(self):
        dish = self.create_dish(self.dish).json()
        response = self.client.delete(f'/dish/{dish["id"]}', headers=self.headers)
        self.assertTrue(response.ok, response.text)
        deleted_dish = response.json()
        self.assertEqual(dish, deleted_dish)
        response = self.client.get('/dishes')
        self.assertTrue(response.ok, response.text)
        self.assertEqual(response.json(), [])

    def test_create_card(self):
        creation_time = self.time_point[0]
        with freeze_time(creation_time):
            response = self.client.post('/card', json=self.card, headers=self.headers)
            self.assertTrue(response.ok, response.text)
        card = response.json()
        response = self.client.get(f'/card/{card["id"]}')
        self.assertTrue(response.ok, response.text)
        db_card = response.json()
        self.assertEqual(card, db_card)
        self.assertIsSubset(card, self.card)
        self.assertIsSubset(db_card, self.card)
        self.assertEqual(datetime.fromisoformat(db_card['creation_time']), creation_time)

    def test_update_card(self):
        creation_time = self.time_point[0]
        update_time = self.time_point[1]

        with freeze_time(creation_time):
            response = self.client.post('/card', json=self.card, headers=self.headers)
            self.assertTrue(response.ok, response.text)
            card = response.json()

        with freeze_time(update_time):
            update = {'name': 'new name', 'description': 'new description'}
            response = self.client.put(f'/card/{card["id"]}', json=update, headers=self.headers)
            self.assertTrue(response.ok, response.text)
            updated_card = response.json()

        self.assertNotEqual(card, updated_card)
        self.assertEqual(update['name'], updated_card['name'])
        self.assertEqual(update['description'], updated_card['description'])
        self.assertEqual(datetime.fromisoformat(updated_card['creation_time']), creation_time)
        self.assertEqual(datetime.fromisoformat(updated_card['update_time']), update_time)

    def test_create_existing_card(self):
        """Create the same card twice, verify if 2nd failed as expected."""
        self.client.post('/card', json=self.card, headers=self.headers)
        response = self.client.post('/card', json=self.card, headers=self.headers)
        self.assertEqual(response.status_code, 409, response.text)
        self.assertEqual(response.json()['detail'], f"Card already exists: {self.id()}")

    def test_create_and_delete_card_with_dishes(self):
        create_time = self.time_point[0]
        update_time = self.time_point[1]
        with freeze_time(create_time):
            response = self.client.post('/card', json=self.card, headers=self.headers)
        self.assertTrue(response.ok, response.text)
        card = response.json()
        with freeze_time(update_time):
            for _ in range(5):
                data = {**self.dish, 'card_id': card['id']}
                response = self.client.post('/dish', json=data, headers=self.headers)
                dish = response.json()
                card['dishes'].append(dish)
        response = self.client.get(f'/card/{card["id"]}')
        self.assertTrue(response.ok, response.text)
        db_card = response.json()
        card['update_time'] = update_time.isoformat()  # Fake update time for comparison
        self.assertEqual(card, db_card)

        response = self.client.delete(f'/card/{card["id"]}', headers=self.headers)
        self.assertTrue(response.ok, response.text)
        deleted_card = response.json()

        self.assertEqual(db_card, deleted_card)
        response = self.client.get(f'/card/{card["id"]}')
        self.assertFalse(response.ok, response.text)
