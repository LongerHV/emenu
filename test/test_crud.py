from datetime import datetime, timedelta
import io
import os
import random
import secrets
from typing import Dict
from unittest.mock import mock_open, patch

from fastapi.exceptions import HTTPException
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from freezegun.api import freeze_time
from jose.exceptions import JWTError
from sqlalchemy.orm.session import Session

from emenu import api_enum, crud, database, errors, schemas
from test.test_common import TestCommon


def strip_none(d: Dict) -> Dict:
    return dict(filter(lambda x: x[1] is not None, d.items()))


class CRUDTestCase(TestCommon):
    def setUp(self):
        super().setUp()
        self.db: Session = database.SessionLocal()

    @property
    def dish_create(self):
        return schemas.DishCreate(**self.dish)

    @property
    def card_create(self):
        return schemas.CardCreate(**self.card)

    @property
    def user_create(self):
        return schemas.UserCreate(**self.user)

    @property
    def random_hex(self):
        return secrets.token_hex(8)

    def test_token_decode(self):
        user = crud.create_user(self.db, self.user_create)
        data = {'sub': user.username}
        token = crud.create_access_token(data)
        data_decoded = crud.decode_token(token)
        self.assertIsSubset(data_decoded, data)

    def test_invalid_token_decode(self):
        data = {'sub': 'some_username'}
        token = crud.create_access_token(data)
        invalid_token = token[:-10]
        with self.assertRaises(JWTError):
            crud.decode_token(invalid_token)

    def test_token(self):
        user_create = self.user_create
        user = crud.create_user(self.db, user_create)
        request_form = OAuth2PasswordRequestForm(
            username=user_create.username, password=user_create.password, scope=''
        )
        access_token = crud.login_for_access_token(self.db, request_form)
        user_from_token = crud.get_user_from_token(self.db, access_token['access_token'])
        self.assertEqual(user, user_from_token)

    def test_create_user(self):
        user = self.user_create
        created_user = crud.create_user(self.db, user)
        self.assertEqual(created_user.username, user.username)
        self.assertEqual(created_user.email, user.email)
        self.assertTrue(
            crud.verify_password(created_user.hashed_password, user.password)
        )

    def test_create_existing_user(self):
        user = self.user_create
        crud.create_user(self.db, user)
        with self.assertRaises(errors.UserExistsError):
            crud.create_user(self.db, user)

        user2 = self.user_create
        user2.username = 'unique_username'
        with self.assertRaises(errors.UserExistsError):
            crud.create_user(self.db, user2)

    def test_update_user(self):
        user = self.user_create
        created_user = crud.create_user(self.db, user)

        original_hashed_password = created_user.hashed_password
        user_update = schemas.UserUpdate(
            username='new_username',
            password='new_password',
            email='new@email.com'
        )
        updated_user = crud.update_user(self.db, user.username, user_update)

        self.assertNotEqual(user.username, updated_user.username)
        self.assertNotEqual(user.email, updated_user.email)
        self.assertNotEqual(original_hashed_password, updated_user.hashed_password)

        self.assertEqual(user_update.username, updated_user.username)
        self.assertEqual(user_update.email, updated_user.email)
        self.assertTrue(
            crud.verify_password(updated_user.hashed_password, str(user_update.password))
        )

    def test_verify_password(self):
        password = self.user_create.password
        hash_ = crud.hash_password(password)
        self.assertTrue(crud.verify_password(hash_, password))
        self.assertFalse(crud.verify_password(hash_, 'wrong_password'))

    def test_create_dish(self):
        dish = self.dish_create
        created_dish = crud.create_dish(self.db, dish)
        self.assertIsSubset(created_dish.__dict__, dish.dict())

    def test_create_dish_for_non_existing_card(self):
        dish = self.dish_create
        setattr(dish, 'card_id', 1)
        with self.assertRaises(HTTPException):
            crud.create_dish(self.db, dish)

    def test_get_dish(self):
        dish = self.dish_create
        dish_id = crud.create_dish(self.db, dish).id
        created_dish = crud.get_dish(self.db, dish_id)
        self.assertIsNotNone(created_dish)
        self.assertIsSubset(created_dish.__dict__, dish.dict())

    def test_get_non_existing_dish(self):
        for i in (-1, 0, 1):
            with self.assertRaises(HTTPException):
                crud.get_dish(self.db, i)

    def test_update_dish(self):
        card_creation_time = self.time_point[0]
        dish_creation_time = self.time_point[1]
        dish_update_time = self.time_point[2]

        card = self.card_create
        with freeze_time(card_creation_time):
            card_id = crud.create_card(self.db, card).id

        dish = self.dish_create
        dish.card_id = card_id
        with freeze_time(dish_creation_time):
            dish_id = crud.create_dish(self.db, dish).id

        card = crud.get_card(self.db, card_id)
        assert card is not None
        self.assertEqual(card.creation_time, card_creation_time)
        self.assertEqual(card.update_time, dish_creation_time)

        dish_update = schemas.DishUpdate(price=10.99, vegetarian=True)
        with freeze_time(dish_update_time):
            crud.update_dish(self.db, dish_id, dish_update)

        card = crud.get_card(self.db, card_id)
        assert card is not None
        self.assertEqual(card.creation_time, card_creation_time)
        self.assertEqual(card.update_time, dish_update_time)

        updated_dish = crud.get_dish(self.db, dish_id)
        self.assertIsSubset(
            updated_dish.__dict__,
            {**dish.dict(), **strip_none(dish_update.dict())}
        )

    def test_delete_dish(self):
        dish = self.dish_create
        dish_id = crud.create_dish(self.db, dish).id
        crud.delete_dish(self.db, dish_id)
        with self.assertRaises(HTTPException):
            crud.get_dish(self.db, dish_id)

    def test_delete_non_existing_dish(self):
        with self.assertRaises(HTTPException):
            crud.delete_dish(self.db, 1)

    def test_create_card(self):
        card = self.card_create
        created_card = crud.create_card(self.db, card)
        self.assertEqual(card.name, created_card.name)
        self.assertEqual(card.description, created_card.description)

    def test_create_existing_card(self):
        card = self.card_create
        crud.create_card(self.db, card)
        with self.assertRaises(HTTPException):
            crud.create_card(self.db, card)

    def test_create_card_with_dishes(self):
        count = 5
        card = self.card_create
        card.dishes = [self.dish_create for _ in range(count)]
        created_card = crud.create_card(self.db, card)
        self.assertEqual(len(created_card.dishes), count)

    def test_create_card_and_add_dishes(self):
        card = self.card_create
        card = crud.create_card(self.db, card)
        dish = self.dish_create
        setattr(dish, 'card_id', card.id)
        for _ in range(5):
            crud.create_dish(self.db, dish)
        card = crud.get_card(self.db, card.id)
        self.assertIsNotNone(card)
        self.assertEqual(len(card.dishes), 5)  # type: ignore

    def test_get_card_by_name(self):
        card = self.card_create
        card = crud.create_card(self.db, card)
        db_card = crud.get_card_by_name(self.db, card.name)
        self.assertIsNotNone(db_card)
        self.assertEqual(card, db_card)

    def test_get_card_by_non_existing_name(self):
        card = crud.get_card_by_name(self.db, 'Some_name')
        self.assertIsNone(card)

    def test_delete_card(self):
        card = self.card_create
        card_id = crud.create_card(self.db, card).id
        crud.delete_card(self.db, card_id)
        deleted_card = crud.get_card(self.db, card_id)
        self.assertIsNone(deleted_card)

    def test_delete_card_with_dishes(self):
        card = self.card_create
        card.dishes = [self.dish_create for _ in range(5)]
        card = crud.create_card(self.db, card)

        card_detail = crud.get_card(self.db, card.id)
        self.assertIsNotNone(card_detail)
        dishes = crud.get_dishes(self.db, card_id=card.id)
        self.assertEqual(card_detail.dishes, dishes)  # type: ignore

        crud.delete_card(self.db, card.id)
        self.assertEqual(crud.get_dishes(self.db), [])

    def test_get_cards(self):
        count = 10
        for i in range(count):
            card = self.card_create
            card.name += self.random_hex  # Make name unique
            card_id = crud.create_card(self.db, card).id
            dish = self.dish_create
            dish.card_id = card_id
            crud.create_dish(self.db, dish)

        cards = crud.get_cards(self.db)
        self.assertEqual(count, len(cards))

        # Try adding empty card
        crud.create_card(self.db, self.card_create)
        cards = crud.get_cards(self.db)
        self.assertEqual(count, len(cards))

    def test_get_cards_invalid_enum(self):
        with self.assertRaises(ValueError):
            crud.get_cards(self.db, order_by='invalid_order')

    def test_get_cards_order_by(self):
        count = 10
        dish_counts = list(range(count))
        random.shuffle(dish_counts)
        for dish_count in dish_counts:
            card = self.card_create
            card.name += self.random_hex  # Make name unique
            card_id = crud.create_card(self.db, card).id
            for _ in range(dish_count + 1):
                dish = self.dish_create
                dish.card_id = card_id
                crud.create_dish(self.db, dish)

        # Sort by name
        cards = crud.get_cards(self.db, order_by=api_enum.CardOrderBy.NAME)
        self.assertEqual(count, len(cards))
        sorted_cards = sorted(cards, key=lambda x: x.name)
        self.assertEqual(cards, sorted_cards)

        # Sort by dish count (reversed)
        cards = crud.get_cards(self.db, order_by=api_enum.CardOrderBy.DISH_COUNT, descending=True)
        self.assertEqual(count, len(cards))
        sorted_cards = sorted(cards, key=lambda x: x.dishes, reverse=True)
        self.assertEqual(cards, sorted_cards)

    def test_get_cards_filter(self):
        count = 10
        dates = [datetime(2020, 1, i) for i in range(1, 1 + count)]
        for date in dates:
            card = self.card_create
            card.name += self.random_hex
            with freeze_time(date):
                card_id = crud.create_card(self.db, card).id
                dish = self.dish_create
                dish.card_id = card_id
                crud.create_dish(self.db, dish)

        skip = 3
        created_after = dates[skip] - timedelta(hours=1)
        created_before = dates[count - skip]
        cards = crud.get_cards(self.db, created_after=created_after)
        self.assertEqual(len(cards), count - skip)

        cards = crud.get_cards(self.db, created_before=created_before)
        self.assertEqual(len(cards), count - skip)

        cards = crud.get_cards(self.db, created_after=created_after, created_before=created_before)
        self.assertEqual(len(cards), count - 2 * skip)

        card = cards[0]
        card_update = schemas.CardUpdate(description='New description')
        with freeze_time(datetime(2021, 1, 1)):
            crud.update_card(self.db, card.id, card_update)

        before_update_time = datetime(2020, 12, 1)
        cards = crud.get_cards(self.db, updated_after=before_update_time)
        self.assertEqual(len(cards), 1)

        cards = crud.get_cards(self.db, updated_before=before_update_time)
        self.assertEqual(len(cards), count - 1)

    def test_delete_non_existing_card(self):
        with self.assertRaises(HTTPException):
            crud.delete_card(self.db, 1)

    def test_images(self):
        filename = 'test.jpg'
        filepath = os.path.join('images', filename)
        dish = crud.create_dish(self.db, self.dish_create)
        content = b'example data mimicking file'
        file = io.BytesIO(content)
        with patch('builtins.open', mock_open(read_data=content)) as mock_file:
            crud.upload_image(self.db, dish.id, filename, file)
            mock_file.assert_called_with(filepath, 'wb')

            with patch('os.path.isfile', side_effect=lambda path: path == filepath) as mock:
                new_filepath = crud.get_image(self.db, dish.id)
                mock.assert_called_with(filepath)
            self.assertEqual(filepath, new_filepath)

            with patch('os.remove', side_effect=lambda path: path == filepath) as mock:
                crud.delete_image(self.db, dish.id)
                mock.assert_called_with(filepath)
