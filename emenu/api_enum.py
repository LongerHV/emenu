from enum import Enum


class CardOrderBy(str, Enum):
    NAME = 'name'
    DISH_COUNT = 'count'
