from datetime import datetime
from importlib.metadata import version
import sys
from typing import Generator, List, Optional

from fastapi import Depends, FastAPI, Path, Query
from fastapi.datastructures import UploadFile
from fastapi.exceptions import HTTPException
from fastapi.param_functions import File
from fastapi.responses import RedirectResponse
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from starlette.responses import FileResponse
import uvicorn

from . import api_enum, config, crud, errors, mail, models, schemas
from .database import SessionLocal, engine


try:
    __version = version(__package__)
except Exception:
    __version = 'dev'


def create_tables():
    """Initial creation of tables in database."""
    models.Base.metadata.create_all(bind=engine)


def create_default_user():
    """Create a default API user if does not already exist."""
    user = schemas.UserCreate(
        username=config.DEFAULT_USER, password=config.DEFAULT_PASS, email=config.DEFAULT_MAIL
    )
    with SessionLocal() as db:
        try:
            crud.create_user(db, user)
        except errors.UserExistsError:
            pass


tags_metadata = [
    {
        'name': 'General',
    },
    {
        'name': 'Users',
        'description': 'Manage users with access to the API',
    },
    {
        'name': 'Cards',
        'description': 'Manage menu cards',
    },
    {
        'name': 'Dishes',
        'description': 'Manage dishes in cards',
    },
]

app = FastAPI(
    title=__package__,
    description='API for browsing and managing restaurant cards',
    version=__version,
    on_startup=[create_tables, create_default_user, mail.schedule_next_email_send],
    openapi_tags=[
        {
            'name': 'General',
        },
        {
            'name': 'Users',
            'description': 'Manage users with access to the API',
        },
        {
            'name': 'Cards',
            'description': 'Manage menu cards',
        },
        {
            'name': 'Dishes',
            'description': 'Manage dishes in cards',
        },
    ],
)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl='token')


def get_db() -> Generator[Session, None, None]:
    """Database dependency generator."""
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def get_current_user(
    db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)
) -> models.User:
    """Decode a token to get currently logged user."""
    return crud.get_user_from_token(db, token)


@app.get('/', tags=['General'])
def read_root():
    """Redirect to docs."""
    return RedirectResponse(url='/docs/')


@app.get('/info', tags=['General'])
def info():
    """Obtain server details."""
    return {'Python': sys.version, 'version': version(__package__), 'SQL driver': engine.driver}


@app.post('/send', tags=['General'])
def send_emails_now(
    current_user: schemas.User = Depends(get_current_user), db: Session = Depends(get_db)
):
    """Send emails with changes now."""
    if config.SMTP_ENABLE:
        mail.send_emails(db)
    else:
        raise HTTPException(400, 'SMTP is not configured')


@app.post('/token', tags=['Users'], response_model=schemas.Token)
def login_for_access_token(
    form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)
):
    """Use username and password to login and obtain authentication token."""
    return crud.login_for_access_token(db, form_data)


@app.post('/user', tags=['Users'], response_model=schemas.User)
def create_user(
    user: schemas.UserCreate,
    current_user: schemas.User = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    """Create a new user."""
    return crud.create_user(db, user)


@app.get('/users/me', tags=['Users'], response_model=schemas.User)
def get_me(current_user: schemas.User = Depends(get_current_user)):
    """Get information about currently logged user (token owner)"""
    return current_user


@app.get('/users/{username}', tags=['Users'], response_model=schemas.User)
def get_user(
    username: str = Path(..., description='Username to find user by'),
    current_user: schemas.User = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    """Get information about a specific user."""
    user = crud.get_user(db, username)
    if user is None:
        raise errors.UserNotFoundError(username)
    return user


@app.put('/users/me', tags=['Users'], response_model=schemas.User)
def update_user(
    user: schemas.UserUpdate,
    current_user: schemas.User = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    """Update user information."""
    return crud.update_user(db, current_user.username, user)


@app.post('/dish', tags=['Dishes'], response_model=schemas.Dish)
def create_dish(
    dish: schemas.DishCreate,
    current_user: schemas.User = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    """Create a new dish."""
    return crud.create_dish(db, dish)


@app.put('/dish/{dish_id}', tags=['Dishes'], response_model=schemas.Dish)
def update_dish(
    dish: schemas.DishUpdate,
    dish_id: int = Path(..., ge=0, description='ID of dish to update'),
    current_user: schemas.User = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    """Modify an existing dish."""
    return crud.update_dish(db, dish_id, dish)


@app.post('/dish-image/{dish_id}', tags=['Dishes'])
def upload_image(
    dish_id: int = Path(..., ge=0, description='ID of dish'),
    file: UploadFile = File(..., description='Picture of the dish', content_type='image/jpeg'),
    current_user: schemas.User = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    """Upload a picture of the dish."""
    return crud.upload_image(db, dish_id, file.filename, file.file)


@app.get('/dish-image/{dish_id}', tags=['Dishes'])
def get_image(
    dish_id: int = Path(..., ge=0, description='ID of dish'), db: Session = Depends(get_db)
):
    """Get dish photo."""
    file_path = crud.get_image(db, dish_id)
    return FileResponse(file_path)


@app.delete('/dish-image/{dish_id}', tags=['Dishes'])
def delete_image(
    dish_id: int = Path(..., ge=0, description='ID of dish'),
    current_user: schemas.User = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    """Delete dish photo."""
    return crud.delete_image(db, dish_id)


@app.get('/dish/{dish_id}', tags=['Dishes'], response_model=schemas.Dish)
def get_dish(
    dish_id: int = Path(..., ge=0, description='ID of the dish to view'),
    db: Session = Depends(get_db),
):
    """Get information about a specific dish."""
    dish = crud.get_dish(db, dish_id)
    return dish


@app.get('/dishes', tags=['Dishes'], response_model=List[schemas.Dish])
def get_dishes(
    card_id: Optional[int] = Query(None, description='ID of card containing the dish'),
    db: Session = Depends(get_db),
):
    """Get list of dishes."""
    return crud.get_dishes(db, card_id=card_id)


@app.delete('/dish/{dish_id}', tags=['Dishes'], response_model=schemas.Dish)
def delete_dish(
    dish_id: int = Path(None, ge=0, description='ID of dish to delete'),
    current_user: schemas.User = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    """Delete a specific dish."""
    return crud.delete_dish(db, dish_id)


@app.post('/card', tags=['Cards'], response_model=schemas.CardDetail)
def create_card(
    card: schemas.CardCreate,
    current_user: schemas.User = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    """Create a new, empty menu card."""
    return crud.create_card(db, card)


@app.put('/card/{card_id}', tags=['Cards'], response_model=schemas.CardDetail)
def update_card(
    card: schemas.CardUpdate,
    card_id: int = Path(..., ge=0, description='ID of the card to update'),
    current_user: schemas.User = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    """Modify an existing menu card."""
    return crud.update_card(db, card_id, card)


@app.get('/card/{card_id}', tags=['Cards'], response_model=schemas.CardDetail)
def get_card(
    card_id: int = Path(..., ge=0, description='ID of the card to view'),
    db: Session = Depends(get_db),
):
    """Get details of a specific card."""
    card = crud.get_card(db, card_id)
    if card is None:
        raise errors.CardNotFoundError(f'id={card_id}')
    return card


@app.get('/cards', tags=['Cards'], response_model=List[schemas.Card])
def get_cards(
    name: Optional[str] = Query(None, description='Exact card name (optional)'),
    order_by: Optional[api_enum.CardOrderBy] = Query(None, description='Enable sorting'),
    descending: Optional[bool] = Query(
        False, description='Descending order (optional, assumes False)'
    ),
    created_after: Optional[datetime] = Query(None, description='Show dishes created after date'),
    created_before: Optional[datetime] = Query(None, description='Show cards created before date'),
    updated_after: Optional[datetime] = Query(None, description='Show cards updated after date'),
    updated_before: Optional[datetime] = Query(None, description='Show cards updated before date'),
    db: Session = Depends(get_db),
):
    """Browse menu cards."""
    return crud.get_cards(
        db,
        name=name,
        order_by=order_by,
        descending=descending or False,
        created_before=created_before,
        created_after=created_after,
        updated_before=updated_before,
        updated_after=updated_after,
    )


@app.delete('/card/{card_id}', tags=['Cards'], response_model=schemas.CardDetail)
def delete_card(
    card_id: int = Path(..., ge=0, description='ID of the card to delete'),
    current_user: schemas.User = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    """Delete a specific card with all it's dishes."""
    return crud.delete_card(db, card_id)


if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=8080)  # type: ignore  # nosec
