from typing import Union

from fastapi.exceptions import HTTPException


class DishNotFoundError(HTTPException):
    def __init__(self, dish: Union[str, int]):
        super().__init__(404, f'Dish not found: {dish}')


class CardNotFoundError(HTTPException):
    def __init__(self, card: Union[str, int]):
        super().__init__(404, f'Card not found: {card}')


class CardExistsError(HTTPException):
    def __init__(self, card: Union[str, int]):
        super().__init__(409, f'Card already exists: {card}')


class UserNotFoundError(HTTPException):
    def __init__(self, user: Union[str, int]):
        super().__init__(404, f'User not found: {user}')


class UserExistsError(HTTPException):
    def __init__(self, username: Union[str, int]):
        super().__init__(409, f'Username already exists: {username}')


class ImageFileExistsError(HTTPException):
    def __init__(self, filename: str):
        super().__init__(409, f'File alreadi exists: {filename}')


class ImageFileNotFoundError(HTTPException):
    def __init__(self, filename: str):
        super().__init__(404, f'Image file not found: {filename}')


class NoImageError(HTTPException):
    def __init__(self, dish: Union[str, int]):
        super().__init__(404, f'Dish has no image: {dish}')


class LoginFailedError(HTTPException):
    def __init__(self):
        super().__init__(
            status_code=401,
            detail='Incorrect username or password',
            headers={'WWW-Authenticate': 'Bearer'},
        )
