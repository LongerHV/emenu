from datetime import datetime
from decimal import Decimal
from typing import List, Optional

from pydantic import BaseModel
from pydantic.networks import EmailStr
from pydantic.types import ConstrainedDecimal, NonNegativeInt


class NonNegativeDecimal(ConstrainedDecimal):
    """Type definition for non-negative decimal values."""
    ge = Decimal(0.0)


class UserBase(BaseModel):
    username: Optional[str] = None
    email: Optional[EmailStr] = None


class UserCreate(UserBase):
    username: str
    email: EmailStr
    password: str

    class Config:
        schema_extra = {
            'example': {
                'username': 'sample_user',
                'password': 'Passw0rd1',
                'email': 'sample@email.com',
            }
        }


class UserUpdate(UserBase):
    password: Optional[str] = None

    class Config:
        schema_extra = {
            'example': {
                'username': 'sample_user',
                'password': 'Passw0rd1',
                'email': 'sample@email.com',
            }
        }


class User(UserBase):
    id: str
    username: str
    email: EmailStr
    hashed_password: str

    class Config:
        orm_mode = True


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str


class DishBase(BaseModel):
    name: str
    description: Optional[str] = ''
    price: NonNegativeDecimal
    vegetarian: Optional[bool] = False
    card_id: Optional[int] = None
    preparation_time: NonNegativeInt
    filename: Optional[str] = None


class DishCreate(DishBase):
    class Config:
        schema_extra = {
            'example': {
                'name': 'Spaghetti',
                'description': 'Italian pasta goodness',
                'price': 12.99,
                'vegetarian': False,
                'preparation_time': 30,
                'card_id': 1,
            }
        }


class DishUpdate(BaseModel):
    name: Optional[str]
    description: Optional[str]
    price: Optional[NonNegativeDecimal]
    vegetarian: Optional[bool]
    card_id: Optional[int]
    preparation_time: Optional[NonNegativeInt]

    class Config:
        schema_extra = {
            'example': {
                'name': 'Spaghetti',
                'description': 'Italian pasta goodness',
                'price': 12.99,
                'vegetarian': False,
                'preparation_time': 30,
                'card_id': 1,
            }
        }


class Dish(DishBase):
    id: str
    creation_time: datetime
    update_time: datetime

    class Config:
        orm_mode = True


class CardBase(BaseModel):
    name: str
    description: Optional[str] = ''


class CardCreate(CardBase):
    dishes: Optional[List[DishCreate]]

    class Config:
        schema_extra = {
            'example': {
                'name': 'Italian menu',
                'description': 'Selection of Italian dishes',
                'dishes': [
                    {
                        'name': 'Spaghetti',
                        'price': 14.99,
                        'preparation_time': 45,
                    }
                ],
            }
        }


class CardUpdate(BaseModel):
    name: Optional[str]
    description: Optional[str]

    class Config:
        schema_extra = {
            'example': {
                'name': 'Italian menu',
                'description': 'Selection of Italian dishes',
            }
        }


class Card(CardBase):
    id: str
    creation_time: datetime
    update_time: datetime
    dishes: int

    class Config:
        orm_mode = True


class CardDetail(CardBase):
    id: str
    creation_time: datetime
    update_time: datetime
    dishes: List[Dish] = []

    class Config:
        orm_mode = True
