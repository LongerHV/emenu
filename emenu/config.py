from datetime import time
import os


# Token signing
SECRET_KEY = os.getenv(
    'EMENU_SECRET_KEY', '6cd130ab8fcecffc212a1b8634ce1be63f264af7e350844c0c5c4b61febee70e'
)
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30

# Postgres settings
PG_ENABLE = bool(os.getenv('EMENU_PG_ENABLE'))
PG_USERNAME = os.getenv('EMENU_PG_USER', 'admin')
PG_PASSWORD = os.getenv('EMENU_PG_PASS', 'admin')
PG_HOST = os.getenv('EMENU_PG_HOST', 'localhost')
PG_PORT = int(os.getenv('EMENU_PG_PORT', '5432'))

# Sending e-mails
SMTP_ENABLE = bool(os.getenv('EMENU_SMTP_ENABLE'))
SMTP_HOST = os.getenv('EMENU_SMTP_HOST', 'localhost')
SMTP_PORT = int(os.getenv('EMENU_SMTP_PORT', '1025'))
SMTP_USER = os.getenv('EMENU_SMTP_USER')
SMTP_PASS = os.getenv('EMENU_SMTP_PASS')
SMTP_SENDER = os.getenv('EMENU_SMTP_SENDER', 'robot@email.com')
MAIL_TIME = time.fromisoformat(os.getenv('EMENU_MAIL_TIME', '10:00:00'))

# Default user
DEFAULT_USER = os.getenv('EMENU_DEFAULT_USER', 'admin')
DEFAULT_PASS = os.getenv('EMENU_DEFAULT_PASS', 'admin')
DEFAULT_MAIL = os.getenv('EMENU_DEFAULT_MAIL', 'admin@admin.admin')
