import asyncio
from datetime import datetime, timedelta
from email.message import EmailMessage
import json
import smtplib

from sqlalchemy.orm.session import Session

from emenu import schemas

from . import config, crud, database


def create_message(db: Session):
    """Get mailing list and update data from db and create a message."""
    msg = EmailMessage()
    msg['To'] = crud.get_mailing_list(db)
    msg['Subject'] = 'emenu update'
    msg['From'] = config.SMTP_SENDER

    now = datetime.utcnow()
    start_time = datetime.combine(now, config.MAIL_TIME)
    if start_time > now:
        start_time -= timedelta(days=1)
    cards = crud.get_cards(db, created_after=start_time)
    content = [schemas.Card(**card).json() for card in cards]  # type: ignore
    msg.set_content(json.dumps(content, indent=2))

    return msg


def send_emails(db: Session):
    """Send emails with card and dish updates."""
    msg = create_message(db)

    mail_server = smtplib.SMTP(config.SMTP_HOST, config.SMTP_PORT)
    username = config.SMTP_USER
    password = config.SMTP_PASS

    if username is not None and password is not None:
        mail_server.ehlo()
        mail_server.starttls()
        mail_server.ehlo()
        mail_server.login(username, password)

    mail_server.send_message(msg)
    mail_server.quit()


def get_time_remaining() -> float:
    """Get seconds remaining to next message."""
    now = datetime.utcnow()
    send_time = datetime.combine(now, config.MAIL_TIME)
    if send_time < now:
        send_time += timedelta(days=1)
    time_remaining = send_time - now
    return time_remaining.total_seconds()


def schedule_next_email_send():
    """Schedule new coroutine call on an event loop."""
    loop = asyncio.get_running_loop()
    when = loop.time() + get_time_remaining()
    loop.call_at(when, send_emails_task)


def send_emails_task():
    """Send emails and schedule next send."""
    with database.SessionLocal() as db:
        send_emails(db)
    schedule_next_email_send()
