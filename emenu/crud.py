"""Reusable functions to interact with the data in database."""
from datetime import datetime, timedelta
import operator
import os
from tempfile import SpooledTemporaryFile
from typing import Any, Callable, Dict, IO, List, Mapping, Optional, Union

from argon2 import PasswordHasher
from argon2.exceptions import VerificationError, VerifyMismatchError
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from jose import jwt
from jose.exceptions import JWTError
from sqlalchemy.orm import Session
from sqlalchemy.orm.query import Query
from sqlalchemy.sql.expression import asc, desc
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.schema import Column

from . import api_enum, config, errors, models, schemas


def verify_password(hash_: str, password: str) -> bool:
    """Check if hash was created with a given password."""
    hasher = PasswordHasher()
    try:
        return hasher.verify(hash_, password)
    except (VerifyMismatchError, VerificationError):
        return False


def hash_password(password: str) -> str:
    """Use Argon2 to hash a password."""
    hasher = PasswordHasher()
    return hasher.hash(password)


def create_access_token(data: Dict):
    """Create an access token."""
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=config.ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({'exp': expire})
    encoded_jwt = jwt.encode(to_encode, config.SECRET_KEY, algorithm=config.ALGORITHM)
    return encoded_jwt


def decode_token(token: str) -> Mapping:
    """Decode a token."""
    return jwt.decode(token, config.SECRET_KEY, algorithms=[config.ALGORITHM])


def get_user_from_token(db: Session, token: str) -> models.User:
    """Decode a token to check if it is valid. Return a user."""
    try:
        payload = decode_token(token)
        username = payload.get('sub')
        if username is None:
            raise errors.LoginFailedError()
        token_data = schemas.TokenData(username=username)
        user = get_user(db, token_data.username)
        if user is None:
            raise errors.LoginFailedError()
        return user
    except JWTError as ex:
        raise errors.LoginFailedError() from ex


def login_for_access_token(db: Session, form_data: OAuth2PasswordRequestForm):
    """Authenticate user with username and password to create an access token."""
    user = get_user(db, form_data.username)
    if user is None:
        raise errors.LoginFailedError()
    if user.hashed_password is None:
        raise errors.LoginFailedError()
    if not verify_password(user.hashed_password, form_data.password):
        raise errors.LoginFailedError()
    access_token = create_access_token(data={'sub': user.username})
    return {'access_token': access_token, 'token_type': 'bearer'}


def get_user(db: Session, username: str) -> Optional[models.User]:
    """Find the user in database using an username."""
    return db.query(models.User).filter(models.User.username == username).first()


def get_user_by_mail(db: Session, email: str) -> Optional[models.User]:
    """Find the user in database using email address."""
    return db.query(models.User).filter(models.User.email == email).first()


def get_mailing_list(db: Session) -> List[str]:
    """Obtain list of all email addressis from the database."""
    results = db.query(models.User.email).all()
    return [value for value, in results]


def set_user_password(user: models.User, password: str):
    """Hash password and set it in user model object."""
    setattr(user, 'hashed_password', hash_password(password))


def create_user(db: Session, user: schemas.UserCreate) -> models.User:
    """Create a new user."""
    verify_user_not_exist(db, user.username, user.email)
    db_user = models.User(username=user.username, email=user.email)
    set_user_password(db_user, user.password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def verify_user_not_exist(db: Session, username: str = None, email: str = None):
    """Raise exception if user already exists."""
    if username is not None and get_user(db, username) is not None:
        raise errors.UserExistsError(username)
    if email is not None and get_user_by_mail(db, email) is not None:
        raise errors.UserExistsError(email)


def update_user(db: Session, username: str, user: schemas.UserUpdate) -> models.User:
    """Update username, password or email of existing user."""
    db_user = get_user(db, username)
    if db_user is None:
        raise errors.UserNotFoundError(username)

    verify_user_not_exist(db, user.username, user.email)

    db_user.update(**user.dict(exclude_none=True, exclude={'password'}))

    if user.password is not None:
        set_user_password(db_user, user.password)

    db.merge(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def update_model_time(model_: Union[models.Card, models.Dish], dt: Optional[datetime] = None):
    """Set update_time to utcnow."""
    setattr(model_, 'update_time', dt or datetime.utcnow())


def upload_image(
    db: Session, dish_id: int, filename: str, file: Union[SpooledTemporaryFile[bytes], IO[Any]]
) -> models.Dish:
    """Upload an image and associate it with a dish."""
    db_dish = get_dish(db, dish_id)
    images_dir = 'images'
    file_path = os.path.join(images_dir, filename)

    if not os.path.isdir(images_dir):
        os.mkdir(images_dir)
    if os.path.isfile(file_path):
        raise errors.ImageFileExistsError(filename)

    with open(file_path, 'wb') as f:
        f.write(file.read())

    db_dish.filename = file_path  # type: ignore
    db.merge(db_dish)
    db.commit()
    db.refresh(db_dish)
    return db_dish


def get_image(db: Session, dish_id: int) -> str:
    """Get image filename."""
    db_dish = get_dish(db, dish_id)
    if db_dish.filename is None:
        raise errors.NoImageError(db_dish.id)
    if not os.path.isfile(db_dish.filename):
        raise errors.ImageFileNotFoundError(db_dish.filename)
    return db_dish.filename


def delete_image_file(filename: str, ex: bool = False):
    """Delete image file. Optionally raise exception if file was not found."""
    if filename is not None:
        try:
            os.remove(filename)
        except FileNotFoundError as e:
            if ex:
                raise errors.ImageFileNotFoundError(filename) from e


def delete_image(db: Session, dish_id: int) -> models.Dish:
    """Delete an image and its assosiation to a dish."""
    db_dish = get_dish(db, dish_id)
    if db_dish.filename is None:
        raise errors.NoImageError(db_dish.id)
    delete_image_file(db_dish.filename, ex=True)
    db_dish.filename = None  # type: ignore
    db.merge(db_dish)
    db.commit()
    db.refresh(db_dish)
    return db_dish


def get_dish(db: Session, dish_id: int) -> models.Dish:
    """Get dish based on id.

    Args:
        db (Session): db
        dish_id (int): dish_id

    Returns:
        models.Dish:
    """
    db_dish = db.query(models.Dish).get(dish_id)
    if db_dish is None:
        raise errors.DishNotFoundError(f'id={dish_id}')
    return db_dish


def get_dishes(db: Session, card_id: Optional[int] = None) -> List[models.Dish]:
    """Get all dishes.

    Args:
        db (Session): db

    Returns:
        List[models.Dish]:
    """
    query = db.query(models.Dish)
    query = optional_filter(query, models.Dish.card_id, operator.eq, card_id)
    return query.all()


def create_dish(db: Session, dish: schemas.DishCreate) -> models.Dish:
    """Create a dish.

    Args:
        db (Session): db
        dish (schemas.DishCreate): dish

    Returns:
        models.Dish:
    """
    now = datetime.utcnow()
    card_id = dish.card_id
    if card_id is not None:
        card = get_card(db, card_id)
        if card is None:
            raise errors.CardNotFoundError(f'{card_id}')
        update_model_time(card, now)

    db_dish = models.Dish(**dish.dict(), creation_time=now, update_time=now)
    db.add(db_dish)
    db.commit()
    db.refresh(db_dish)
    return db_dish


def update_dish(db: Session, dish_id: int, dish: schemas.DishUpdate) -> models.Dish:
    """Modify dish based on given schema. Fields with value equal to None are ignored.

    Args:
        db (Session): db
        dish_id (int): dish_id
        dish (schemas.DishUpdate): dish

    Returns:
        models.Dish: Modified dish
    """
    db_dish = get_dish(db, dish_id)
    db_dish.update(**dish.dict(exclude_none=True))
    now = datetime.utcnow()
    update_model_time(db_dish, now)

    card_id = db_dish.card_id
    db_card = get_card(db, card_id) if card_id else None
    if db_card:
        update_model_time(db_card, now)
        db.merge(db_card)

    db.merge(db_dish)
    db.commit()
    db.refresh(db_dish)
    if db_card:
        db.refresh(db_card)
    return db_dish


def delete_dish(db: Session, dish_id: int) -> models.Dish:
    """Delete a dish with a given id.

    Args:
        db (Session): db
        dish_id (int): dish_id

    Returns:
        models.Dish: Deleted dish
    """
    db_dish = get_dish(db, dish_id)

    if db_dish.card_id is not None:
        card = get_card(db, db_dish.card_id)
        if card is not None:
            update_model_time(card)

    if db_dish.filename is not None:
        delete_image_file(db_dish.filename)

    db.delete(db_dish)
    db.commit()
    return db_dish


def get_card(db: Session, card_id: int) -> Optional[models.Card]:
    """Get card detail with a given id.

    Args:
        db (Session): db
        card_id (int): card_id

    Returns:
        models.Card: Card with all included dishes
    """
    return db.query(models.Card).get(card_id)


def get_card_by_name(db: Session, name: str) -> Optional[models.Card]:
    """Find card based on a given name.

    Args:
        db (Session): db
        name (str): name

    Returns:
        models.Card:
    """
    return db.query(models.Card).filter(models.Card.name == name).first()


def create_card(db: Session, card: schemas.CardCreate) -> models.Card:
    """Create card based on given schema.

    Args:
        db (Session): db
        card (schemas.CardCreate): card

    Returns:
        models.Card: Created card
    """
    if get_card_by_name(db, card.name) is not None:
        raise errors.CardExistsError(card.name)
    now = datetime.utcnow()

    dishes = [
        models.Dish(**dish.dict(), creation_time=now, update_time=now) for dish in card.dishes or []
    ]

    db_card = models.Card(
        **card.dict(exclude={'dishes'}), creation_time=now, update_time=now, dishes=dishes
    )

    db.add(db_card)
    db.commit()
    db.refresh(db_card)
    return db_card


def update_card(db: Session, card_id: int, card: schemas.CardUpdate) -> models.Card:
    """Update card based on given schema.

    Args:
        db (Session): db
        card_id (int): card_id
        card (schemas.CardUpdate): card

    Returns:
        models.Card: Updated card
    """
    new_name = card.dict().get('name', '')
    if get_card_by_name(db, new_name) is not None:
        raise errors.CardExistsError(new_name)

    db_card = get_card(db, card_id)
    if db_card is None:
        raise errors.CardNotFoundError(f'id={card_id}')

    db_card.update(**card.dict(exclude_none=True))
    update_model_time(db_card)

    db.merge(db_card)
    db.commit()
    db.refresh(db_card)
    return db_card


def optional_filter(query: Query, a: Column, op: Callable, b: Any):
    """Apply a query filter only if 'b' is not None."""
    if b is not None:
        return query.filter(op(a, b))
    return query


def sort_cards_query(query: Query, order_by: Optional[str], descending: bool) -> Query:
    """Sort cards by name or dish count."""
    if order_by is None:
        return query

    order_func = desc if descending else asc
    if order_by == api_enum.CardOrderBy.NAME:
        query = query.order_by(order_func(models.Card.name))
    elif order_by == api_enum.CardOrderBy.DISH_COUNT:
        query = query.order_by(order_func(func.count(models.Card.dishes)))
    else:
        raise ValueError(f'{order_by} is not a valid argument for "order_by"')
    return query


def get_cards(
    db: Session,
    name: Optional[str] = None,
    created_after: Optional[datetime] = None,
    created_before: Optional[datetime] = None,
    updated_after: Optional[datetime] = None,
    updated_before: Optional[datetime] = None,
    order_by: Optional[str] = None,
    descending: bool = False,
) -> List[models.Card]:
    """Get a list of non empty cards.

    Args:
        db (Session): db
        name (Optional[str]): name
        order_by (Optional[api_enum.CardOrderBy]): order_by
        descending (bool): descending

    Returns:
        List[models.Card]: List of cards matching given criteria
    """
    query: Query = (
        db.query(
            models.Card.name,
            models.Card.id,
            models.Card.creation_time,
            models.Card.update_time,
            func.count(models.Dish.card_id).label('dishes'),
        )
        .join(models.Dish)
        .group_by(models.Card)
    )

    # Filters
    query = optional_filter(query, models.Card.name, operator.eq, name)
    query = optional_filter(query, models.Card.creation_time, operator.gt, created_after)
    query = optional_filter(query, models.Card.creation_time, operator.lt, created_before)
    query = optional_filter(query, models.Card.update_time, operator.gt, updated_after)
    query = optional_filter(query, models.Card.update_time, operator.lt, updated_before)

    # Order by
    query = sort_cards_query(query, order_by, descending)

    return query.all()


def delete_card(db: Session, card_id: int) -> models.Card:
    """Delete cards with all dishes.

    Args:
        db (Session): db
        card_id (int): card_id

    Returns:
        models.Card: Deleted card with all deleted dishes
    """
    db_card = get_card(db, card_id)
    if db_card is None:
        raise errors.CardNotFoundError(f'id={card_id}')

    # Delete all dishes referencing this card
    query = db.query(models.Dish).filter(models.Dish.card_id == db_card.id)
    dishes = query.all()

    for dish in dishes:
        if dish.filename is not None:
            delete_image_file(dish.filename)
    query.delete()

    db.delete(db_card)
    db.commit()

    # Make sure returned dishes have an original card id
    for dish in dishes:
        setattr(dish, 'card_id', db_card.id)
    setattr(db_card, 'dishes', dishes)
    return db_card
