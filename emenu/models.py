from sqlalchemy import Boolean, Column, DateTime, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.sql.sqltypes import Numeric

from .database import Base


class Updatable:
    """Allow model object to be updated with a dict."""

    def update(self, **kwargs):
        for key, val in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, val)


class User(Base, Updatable):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True, nullable=False)
    email = Column(String, unique=True, index=True, nullable=False)
    hashed_password = Column(String)


class Dish(Base, Updatable):
    __tablename__ = 'dishes'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    description = Column(String)
    creation_time = Column(DateTime)
    update_time = Column(DateTime)
    price = Column(Numeric)
    vegetarian = Column(Boolean)
    preparation_time = Column(Integer)
    card_id = Column(Integer, ForeignKey('cards.id'))
    filename = Column(String)

    card = relationship('Card', back_populates='dishes')


class Card(Base, Updatable):
    __tablename__ = 'cards'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True, index=True, nullable=False)
    description = Column(String)
    creation_time = Column(DateTime)
    update_time = Column(DateTime)

    dishes = relationship('Dish', back_populates='card', uselist=True)
