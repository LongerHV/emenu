import sys

from sqlalchemy import create_engine
from sqlalchemy.engine.base import Engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import StaticPool

from . import config

# Use sqlite in-memory mode for tests only
if 'pytest' in sys.modules:
    SQLALCHEMY_DATABASE_URL = 'sqlite:///:memory:'
    ENGINE_KWARGS = {
        'poolclass': StaticPool,
        'connect_args': {'check_same_thread': False},
    }

elif config.PG_ENABLE:
    SQLALCHEMY_DATABASE_URL = 'postgresql://{}:{}@{}:{}'.format(
        config.PG_USERNAME, config.PG_PASSWORD, config.PG_HOST, config.PG_PORT
    )
    ENGINE_KWARGS = {}

else:
    SQLALCHEMY_DATABASE_URL = 'sqlite:///./emenu.db'
    ENGINE_KWARGS = {
        'connect_args': {'check_same_thread': False},
    }

engine: Engine = create_engine(SQLALCHEMY_DATABASE_URL, **ENGINE_KWARGS)  # type: ignore
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
