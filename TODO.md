# TODO

## API

- [x] AUTH
- [x] Create card/dish
- [x] Update card/dish
- [x] Delete card/dish
- [x] Get card/dish
- [x] List cards/dishs
- [x] Order cards by name, dish count
- [x] Filter cards/dishs by name
- [x] Filter cards/dishs by dates
- [x] Dishes in cards
- [x] Card detail
- [x] Manage users
- [x] Create card with dishes at once

## DB

- [x] SQLite (default)
- [x] SQLite in-memory (for tests)
- [x] Postgres (for docker-compese and kubernetes)
- [ ] Database migration

## Gitlab CI

- [x] Lint
- [x] Test
- [x] Type check
- [x] Package PyPI
- [x] Package Docker
- [x] Deploy in kubernetes
- [ ] Separate staging deployment (+ ingress and subdomains)

## Docker

- [x] Dockerfile
- [x] docker-compose
- [x] Kubernetes deployment
- [ ] PostgreSQL in kubernetes
- [ ] SSL for Kubernetes

## Other

- [x] README
- [x] Swagger
- [x] Mails
- [x] Coverage
- [x] Initialization data
- [ ] Docstrings for all functions
- [x] More verbose descriptions in SWAGGER
- [x] Store photos
